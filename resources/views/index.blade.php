@extends('layouts.layout')
@section('content')

    <div class="col-sm-12">
        <div class="row">

            @include('partials.sideNav')

            <div class="col-md-10 main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 search-content">
                            <span>Lista de usuarios registrados</span>
                        </div>
                        <a href="#!" class="btn btn-info btn-block col-md-2 create-user" data-toggle="modal" data-target="#createModal">Nuevo <i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table col-sm-12 table-users">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Nombre:</th>
                            <th scope="col">Correo:</th>
                            <th scope="col">Opciones:</th>
                        </tr>
                        </thead>
                        <tbody class="table-body">
                        @foreach($users as $key => $user)
                            <tr class="user-{{ $user->id }}">
                                <th scope="row">{{ $user->id }}</th>
                                <td class="name">{{ $user->name }}</td>
                                <td class="email">{{ $user->email }}</td>
                                <td>
                                    <a href="{{ route('users.show', $user) }}" class="btn btn-info edit-user">editar</a>

                                    <a href="#!" class="btn btn-danger delete" data-id='{{ $user->id }}'>eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}

                </div>

            </div>

        </div>
    </div>

    @include('partials.modal')

@endsection