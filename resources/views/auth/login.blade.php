@extends('layouts.layout')

@section('content')

    <div class="container">
        <div class="row align-items-center justify-content-center login-wrapper">
            <div class="col-md-6 login-box">
                <div class="row justify-content-end">
                    <div class="col-md-6 form-content">
                        <h3>Bienvenido</h3>
                        <small class="form-text text-muted">Sistema de log-in de MarivalGroup.</small>
                        <form action="{{ route('login') }}" method="post" class="form-login">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Usuario</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña:</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group btn-forms">
                                <a href="#!" class="">Cancelar</a>
                                <button class="btn" type="submit">Enviar <i class="fa fa-angle-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
