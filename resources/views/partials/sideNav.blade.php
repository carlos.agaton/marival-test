<div class="col-md-2 side-bar">
    <div class="image-wrapper">
        <div class="blur-avatar"></div>
        <div class="avatar">
            <img src="/images/taylor.jpg" alt="">
        </div>
    </div>
    <div class="profile-info">
        <span>Backend Developer</span>
        <span class="user-email">developer@marivalgroup.com</span>
    </div>
    <ul class="side-menu">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>