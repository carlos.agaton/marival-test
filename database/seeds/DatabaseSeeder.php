<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();

        // $this->call(UsersTableSeeder::class);

        factory(User::class, 1000)->create();

        factory(Transaction::class, 1000)->create();
    }
}
