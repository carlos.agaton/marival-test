(function () {

    $('.form-create').on('submit', function(e) {
        console.log($(this).serialize());
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result){
                var user = jQuery.parseJSON(result);

                var html = '';
                html += '<tr>';
                html += '<th scope="row">'+ user.id +'</th>';
                html += '<td>' + user.name + '</td>';
                html += '<td>' + user.email + '</td>';
                html += '<td>';
                html += '<a href="/users/'+ user.id +'" class="btn btn-info">editar</a>';
                html += '<a href="#!" class="btn btn-danger">eliminar</a>';
                html += '</td>';
                html += '</tr>';

                $('#createModal').modal('hide');
                $('.table-body').prepend(html);

            },
            error: function(){

            }
        });

    });


    $('.form-edit').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'html',
            success: function(result){
                if (result == 'error')
                {
                    swal("El correo ya ha sido tomado", "", "info");
                }
                else
                {
                    var user = jQuery.parseJSON(result);
                    var row = $('.user-'+user.id);
                    row.find('.name').text(user.name);
                    row.find('.email').text(user.email);
                    $('#editModal').modal('hide')

                }
            },
            error: function(){

            }
        });

    });

    $('.edit-user').on('click', function (e) {
        e.preventDefault();

        $.get($(this).attr('href'), function(data, status){
            var form = $('.form-edit');

            form.attr('action', 'users/'+data.id);
            form.find('#name').val(data.name);
            form.find('#email').val(data.email);

            $('#editModal').modal('show');
        });

    });
    
})();