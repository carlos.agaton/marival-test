<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function setNameAttribute($name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    public function setemailAttribute($name)
    {
        $this->attributes['email'] = strtolower($name);
    }

    public function getNameAttribute($name)
    {
        return ucwords($name);
    }

}
